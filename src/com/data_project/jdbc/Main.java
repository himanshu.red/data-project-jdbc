package com.data_project.jdbc;

import java.sql.*;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String url = "jdbc:postgresql://localhost:5432/" + "ipl";
        String username = "postgres";
        String password = "password56";

        Connection con = createConnection(url, username, password);
        runQueries(con);
    }

    public static Connection createConnection(String url, String username, String password) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(url, username, password);
    }

    public static void runQueries(Connection con) throws SQLException {
        String query1 = "SELECT DISTINCT season, COUNT (season) AS matches " +
                "FROM matches " +
                "GROUP BY season ORDER BY season";

        String query2 = "SELECT DISTINCT winner, COUNT(winner) AS wins" +
                " FROM matches " +
                "GROUP BY winner " +
                "HAVING winner IS NOT NULL " +
                "ORDER BY wins DESC";

        String query3 = "SELECT  DISTINCT deliveries.bowling_team, SUM (deliveries.extra_runs) as extra_runs " +
                "FROM deliveries " +
                "LEFT JOIN matches ON matches.id = deliveries.match_id " +
                "WHERE matches.season = '2016' " +
                "GROUP BY deliveries.bowling_team ;";

        String query4 = "SELECT DISTINCT deliveries.bowler, (SUM (deliveries.total_runs) / (COUNT(deliveries.bowler)/6)) as economy " +
                "FROM deliveries LEFT JOIN matches ON deliveries.match_id = matches.id " +
                "WHERE matches.season = '2015' " +
                "GROUP BY deliveries.bowler " +
                "ORDER BY economy ASC " +
                "LIMIT 1 OFFSET 1;";

        Statement statement = con.createStatement();

        ResultSet resultSet = statement.executeQuery(query1);
        System.out.println("No of matches played in each year");
        while (resultSet.next()) {
            String seasons = resultSet.getString("season");
            String matches = resultSet.getString("matches");
            System.out.println(seasons + " " + matches);
        }
        System.out.println();

        resultSet = statement.executeQuery(query2);
        System.out.println("List of teams and no of wins");
        while (resultSet.next())
        {
            String team = resultSet.getString("winner");
            String totalWins = resultSet.getString("wins");
            System.out.println(team + " " + totalWins);
        }
        System.out.println();

        resultSet = statement.executeQuery(query3);
        System.out.println("No of runs conceded by each team in year 2016");
        while (resultSet.next())
        {
            String bowling_team = resultSet.getString("bowling_team");
            String extraRuns = resultSet.getString("extra_runs");
            System.out.println(bowling_team + " " + extraRuns);
        }
        System.out.println();

        resultSet = statement.executeQuery(query4);
        System.out.println("Most economical bowler of year 2015");
        while(resultSet.next())
        {
            String mostEconomicalBowler = resultSet.getString("bowler");
            String bowlingEconomy = resultSet.getString("economy");
            System.out.println(mostEconomicalBowler + " " + bowlingEconomy);
        }
    }
}
